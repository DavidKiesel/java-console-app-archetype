java-console-app-archetype
==========================

# Introduction

Simple Java console application archetype.

# Create This Archetype from Project

I did not have success setting archetype configuration in the prototype's `pom.xml`.

I did have success setting configuration in an `archetype.properties` file in the prototype.

Execute commands below.

```bash
mkdir \
    -p \
    ~/tmp

cd tmp

git \
    clone \
    git@bitbucket.org:DavidKiesel/java-console-app.git

cd java-console-app

mvn \
    clean \
    archetype:create-from-project \
    -Darchetype.properties='archetype.properties'

rm \
    -rf \
    target/generated-sources/archetype/target/

cp \
    -a \
    .gitignore \
    target/generated-sources/archetype/

cp \
    -a \
    .gitignore \
    target/generated-sources/archetype/src/main/resources/archetype-resources/

mv \
    target/generated-sources/archetype \
    ~/tmp/java-console-app-archetype
```

Modify top `pom.xml` properties, description, url.

```xml
    <!-- properties ****************************************************** -->
    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    </properties>

    <description>Simple Java console application archetype.</description>

    <url>https://example.com</url>
```

Modify top `pom.xml` so dotfiles (e.g., `.gitignore`) can be included.

```xml
    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-resources-plugin</artifactId>
                <version>3.0.2</version>

                <configuration>
                    <addDefaultExcludes>false</addDefaultExcludes>
                </configuration>
            </plugin>
        </plugins>
    ...
    </build>
```

Modify top `pom.xml` source control management.

```xml
    <!-- source control management *************************************** -->

    <scm>
        <developerConnection>scm:git:ssh://git@bitbucket.org/DavidKiesel/java-console-app-archetype</developerConnection>
        <url>https://bitbucket.org/DavidKiesel/java-console-app-archetype</url>
    </scm>
```

Modify `src/main/resources/META-INF/maven/archetype-metadata.xml`.  Indicate which files should be filtered through the
Velocity engine and which should not.

In all files that will be processed by the Velocity engine:

1.  Perform substitutions:

    - `$` with `${symbol_dollar}`
    - `\` with `${symbol_escape}`
    - `#` with `${symbol_pound}`
    - `${symbol_dollar}${groupId}` with `${groupId}`
    - `${symbol_dollar}${artifactId}` with `${artifactId}`
    - `${symbol_dollar}${version}` with `${version}`
    - `${symbol_dollar}{project.groupId}.javaconsoleapp` with `${package}`
    - `java-console-app` with `${artifactId}`

2.  Add to top of file:

```
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
#set( $symbol_pound = '#' )
```

This should already have been done automatically for `*.java` files and
resource `*.xml` files.  However, this had to be done manually for the
`README.md` and `pom.xml` under `archetype-resources`.

# Install This Archetype Locally

Execute command.

```bash
mvn \
    clean \
    install
```

# Generate a Project from This Archetype

For example, execute command below.

```bash
mvn \
    archetype:generate \
    -DarchetypeGroupId=net.suddenthought.archetypes \
    -DarchetypeArtifactId=java-console-app-archetype \
    -DarchetypeVersion=1.0.0-SNAPSHOT \
    -DgroupId=net.suddenthought.tests \
    -DartifactId=java-test \
    -Dversion=1.0.0-SNAPSHOT \
    -Dpackage=net.suddenthought.foo \
    -Dorganization=DavidKiesel
```

To generate something like the original source, execute command below.

```bash
mvn \
    archetype:generate \
    -DarchetypeGroupId=net.suddenthought.archetypes \
    -DarchetypeArtifactId=java-console-app-archetype \
    -DarchetypeVersion=1.0.0-SNAPSHOT \
    -DgroupId=com.example \
    -DartifactId=java-console-app \
    -Dversion=1.0.0-SNAPSHOT \
    -Dpackage=com.example.javaconsoleapp \
    -Dorganization=DavidKiesel
```

# Resources

- [Maven Introduction to Archetypes](https://maven.apache.org/guides/introduction/introduction-to-archetypes.html)

- [maven-archtype-plugin](https://maven.apache.org/archetype-archives/archetype-2.3/maven-archetype-plugin/index.html)

- [Velocity User Guide](http://velocity.apache.org/engine/2.0/user-guide.html)

- [Maven archtype directory structure](https://maven.apache.org/archetypes/maven-archetype-archetype/index.html)

- [Maven archetype descriptor (file archetype-metadata.xml)](https://maven.apache.org/archetype/archetype-models/archetype-descriptor/index.html)

- [List of available Maven archetypes](https://maven.apache.org/archetypes/index.html)

- [Create an archetype from a project](https://maven.apache.org/archetype-archives/archetype-2.3/maven-archetype-plugin/create-from-project-mojo.html)

#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
#set( $symbol_pound = '#' )
${artifactId}
================

${symbol_pound} Introduction

Simple Java console application.

Once built, it can be executed with a command like the following:

```bash
java ${symbol_escape}
    -jar target/${artifactId}-*-jar-with-dependencies.jar ${symbol_escape}
    Joe
```
#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package};

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

/**
 * <p>Java application for testing.</p>
 */
public class App
{
    //////////////////////////////////////////////////////////////////////////
    // static fields

    /**
     * <p>This class.</p>
     */
    private static final Class<App> CLASS =
        App.class;

    /**
     * <p>Logger.</p>
     */
    private static final Logger LOGGER =
        LogManager.getLogger(
            CLASS
        );

    //////////////////////////////////////////////////////////////////////////
    // static methods

    /**
     * <p>
     *     Main entry point.
     * </p>
     *
     * @param args  program arguments
     */
    public static void main(
        String[] args
    )
    {
        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("begin main(String[])");

        processArgs(args);

        System.out.println(
            "Hello, world!"
        );

        //////////////////////////////////////////////////////////////////////
        LOGGER.debug("end main(String[])");
    }

    /**
     * <p>
     *     Process arguments.
     * </p>
     *
     * @param args  a {@link String} array
     */
    @SuppressWarnings("WeakerAccess")
    public static void processArgs(
        String[] args
    )
    {
        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("begin processArgs(String[])");

        for (String arg : args)
        {
            System.out.println(
                processArg(arg)
            );
        }

        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("end processArgs(String[])");
    }

    /**
     * <p>
     *     Process an argument.
     * </p>
     *
     * @param arg  a {@link String}
     * @return  {@code Hello, arg!}
     */
    @SuppressWarnings("WeakerAccess")
    public static String processArg(
        String arg
    )
    {
        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("begin processArg(String)");

        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("end processArg(String)");

        return "Hello, " + arg + "!";
    }
}
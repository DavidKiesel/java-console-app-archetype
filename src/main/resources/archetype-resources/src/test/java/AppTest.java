#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package};

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

/**
 * <p>
 *     {@link App} unit tests.
 * </p>
 *
 * <p>
 *     Test method naming convention: {@code productionMethod_stateUnderTest_expectedBehavior}.
 * </p>
 */
@Tag("parallel")
class AppTest
{
    //////////////////////////////////////////////////////////////////////////
    // static fields

    /**
     * <p>This class.</p>
     */
    private static final Class<AppTest> CLASS =
        AppTest.class;

    /**
     * <p>Logger.</p>
     */
    private static final Logger LOGGER =
        LogManager.getLogger(
            CLASS
        );

    //////////////////////////////////////////////////////////////////////////
    // tests

    /**
     * <p>method: {@link App#processArg(String)}</p>
     *
     * <p>state: normal</p>
     *
     * <p>expected: normal</p>
     *
     * @param string  string input
     * @param expectedResult  string output
     */
    @ParameterizedTest
    @CsvSource(
        {
            "world,'Hello, world!'",
            "Tom,'Hello, Tom!'"
        }
    )
    void processArg_normal_normal(
        String string,
        String expectedResult
    )
    {
        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("setup...");

        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("process...");

        String result = App.processArg(
            string
        );

        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("post-process...");

        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("assertions...");

        Assertions.assertEquals(
            expectedResult,
            result,
            "Should return expected String."
        );
    }
}
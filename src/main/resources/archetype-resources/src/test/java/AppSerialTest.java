#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package};

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.junit.jupiter.api.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

/**
 * <p>
 *     {@link App} serial unit tests.
 * </p>
 *
 * <p>
 *     Test method naming convention: {@code productionMethod_stateUnderTest_expectedBehavior}.
 * </p>
 */
@Tag("serial")
class AppSerialTest
{
    //////////////////////////////////////////////////////////////////////////
    // static fields

    /**
     * <p>This class.</p>
     */
    private static final Class<AppSerialTest> CLASS =
        AppSerialTest.class;

    /**
     * <p>Logger.</p>
     */
    private static final Logger LOGGER =
        LogManager.getLogger(
            CLASS
        );

    //////////////////////////////////////////////////////////////////////////
    // instance fields

    /**
     * <p>Original System.out.</p>
     */
    private PrintStream originalSystemOut;

    /**
     * <p>Bytes captured from System.out.</p>
     */
    private ByteArrayOutputStream systemOutContent;

    //////////////////////////////////////////////////////////////////////////
    // instance methods

    /**
     * <p>
     *     Redirect System.out to capture to ByteArrayOutputStream.
     * </p>
     */
    @BeforeEach
    void redirectSystemOutStream()
    {
        originalSystemOut = System.out;

        systemOutContent = new ByteArrayOutputStream();

        System.setOut(
                new PrintStream(systemOutContent)
        );
    }

    /**
     * <p>
     *     Restore original System.out.
     * </p>
     */
    @AfterEach
    void restoreSystemOutStream()
    {
        System.setOut(
                originalSystemOut
        );
    }

    //////////////////////////////////////////////////////////////////////////
    // tests

    /**
     * <p>method: {@link App${symbol_pound}processArgs(String[])}</p>
     *
     * <p>state: normal</p>
     *
     * <p>expected: normal</p>
     */
    @Test
    void processArgs_normal_normal()
    {
        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("setup...");

        String[] args = {
            "Tom"
        };

        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("process...");

        App.processArgs(args);

        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("post-process...");

        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("assertions...");

        Assertions.assertEquals(
                "Hello, Tom!${symbol_escape}n",
                systemOutContent.toString(),
                "System.out should match."
        );
    }
}
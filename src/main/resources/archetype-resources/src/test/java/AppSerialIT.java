#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package};

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.junit.jupiter.api.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

/**
 * <p>
 *     {@link App} serial integration tests.
 * </p>
 *
 * <p>
 *     Test method naming convention: {@code productionMethod_stateUnderTest_expectedBehavior}.
 * </p>
 */
@Tag("serial")
class AppSerialIT
{
    //////////////////////////////////////////////////////////////////////////
    // static fields

    /**
     * <p>This class.</p>
     */
    private static final Class<AppSerialIT> CLASS =
        AppSerialIT.class;

    /**
     * <p>Logger.</p>
     */
    private static final Logger LOGGER =
        LogManager.getLogger(
            CLASS
        );

    //////////////////////////////////////////////////////////////////////////
    // instance fields

    /**
     * <p>Original System.out.</p>
     */
    private PrintStream originalSystemOut;

    /**
     * <p>Bytes captured from System.out.</p>
     */
    private ByteArrayOutputStream systemOutContent;

    //////////////////////////////////////////////////////////////////////////
    // instance methods

    /**
     * <p>
     *     Redirect System.out to capture to ByteArrayOutputStream.
     * </p>
     */
    @BeforeEach
    void redirectSystemOutStream()
    {
        originalSystemOut = System.out;

        systemOutContent = new ByteArrayOutputStream();

        System.setOut(
            new PrintStream(systemOutContent)
        );
    }

    /**
     * <p>
     *     Restore original System.out.
     * </p>
     */
    @AfterEach
    void restoreSystemOutStream()
    {
        System.setOut(
            originalSystemOut
        );
    }

    //////////////////////////////////////////////////////////////////////////
    // tests

    /**
     * <p>method: {@link App${symbol_pound}main(String[])}</p>
     *
     * <p>state: App.propertiesFilename not set</p>
     *
     * <p>expected: normal</p>
     */
    @Test
    void main_normal_normal()
    {
        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("setup...");

        String[] args = {
                "Tom"
        };

        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("process...");

        App.main(args);

        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("post-process...");

        String string = systemOutContent.toString();

        LOGGER.debug(
                string
        );

        LOGGER.debug(
                "{}",
                string.indexOf("${symbol_escape}n")
        );

        String[] strings = string.split(
                "${symbol_escape}n"
        );

        int stringNum = 0;

        //////////////////////////////////////////////////////////////////////
        LOGGER.trace("assertions...");

        Assertions.assertEquals(
                "Hello, Tom!",
                strings[stringNum],
                "System.out should match."
        );

        ++stringNum;
        Assertions.assertEquals(
                "Hello, world!",
                strings[stringNum],
                "System.out should match."
        );

        Assertions.assertEquals(
                2,
                strings.length,
                "Number of lines in System.out should match."
        );
    }
}
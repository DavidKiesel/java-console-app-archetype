#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
#set( $symbol_pound = '#' )
${artifactId}
================

${symbol_pound} Maven

${symbol_pound}${symbol_pound} `javadoc:javadoc`

The command below does the following:

- clean
- build javadoc files

```bash
mvn ${symbol_escape}
    clean ${symbol_escape}
    javadoc:javadoc
```

${symbol_pound}${symbol_pound} `test`

The command below does the following:

- clean
- compile `src/main` classes
- compile `src/test` classes
- execute unit tests tests

```bash
mvn ${symbol_escape}
    clean ${symbol_escape}
    test
```

${symbol_pound}${symbol_pound} `cobertura:cobertura`

The command below does the following:

- clean
- compile `src/main` classes
- compile `src/test` classes
- execute unit tests tests
- generate a Cobertura report

```bash
mvn ${symbol_escape}
    clean ${symbol_escape}
    cobertura:cobertura
```

${symbol_pound}${symbol_pound} `package`

The command below does the following:

- clean
- compile `src/main` classes
- compile `src/test` classes
- execute tests
- build the jar file
- create source jar file
- create javadoc jar file

```bash
mvn ${symbol_escape}
    clean ${symbol_escape}
    package
```

${symbol_pound}${symbol_pound} `exec:java`

The command below does the following:

- clean
- compile classes
- execute tests
- build the jar file
- create source jar file
- executes java main class

```bash
mvn ${symbol_escape}
    clean ${symbol_escape}
    package ${symbol_escape}
    exec:java ${symbol_escape}
    '-Dexec.args=Joe'
```

${symbol_pound}${symbol_pound} `install`

The command below does the following:

- clean
- compile classes
- execute tests
- build the jar file
- create source jar file
- create javadoc jar file
- create checksums on artifacts
- gpg sign artifacts
- install the artifacts in the local repository

```bash
mvn ${symbol_escape}
    clean ${symbol_escape}
    install
```

${symbol_pound}${symbol_pound} `deploy`

The command below does the following:

- clean
- compile classes
- execute tests
- build the jar file
- create source jar file
- create javadoc jar file
- create checksums on artifacts
- gpg sign artifacts
- install the artifact in the local repository
- deploy the artifacts to the libs-snapshot repository of artifactory (assuming element project.version in pom.xml is
  something like 1.2.3-SNAPSHOT)

```bash
git checkout master

mvn ${symbol_escape}
    clean ${symbol_escape}
    deploy
```

${symbol_pound}${symbol_pound} `release:prepare`

The command below does the following:

- clean
- change pom.xml project.version version to release version (e.g., from 1.2.3-SNAPSHOT TO 1.2.3)
- change pom.xml project.scm.tag version to release version (e.g., from master to v1.2.3)
- compile classes
- execute tests
- build the jar file
- gpg sign artifacts
- commit modified POM(s)
- tag latest commit in SCM with release version name
- change pom.xml project.version version to next SNAPSHOT version (e.g., from 1.2.3 to 1.2.4-SNAPSHOT)
- change pom.xml project.scm.tag version to prior value (e.g., from v1.2.3 to master)
- commit modified POM(s)
- push commits to remote

```bash
git checkout master

mvn ${symbol_escape}
    -DscmCommentPrefix='My comment line one.${symbol_dollar}{line.separator}My comment line two.${symbol_dollar}{line.separator}${symbol_dollar}{line.separator}' ${symbol_escape}
    clean ${symbol_escape}
    release:prepare
```

Or something like...

```bash
git checkout master

RELEASE_VERSION=${symbol_dollar}(
    xmllint ${symbol_escape}
        --xpath "//*[local-name()='project']/*[local-name()='version']/text()" ${symbol_escape}
        pom.xml ${symbol_escape}
    |
    sed 's/-SNAPSHOT${symbol_dollar}//'
)

echo "${symbol_dollar}{RELEASE_VERSION}"

mkdir ${symbol_escape}
    -p ${symbol_escape}
    tmp/release_comments

gvim ${symbol_escape}
    "tmp/release_comments/${symbol_dollar}{RELEASE_VERSION}.txt"

mvn ${symbol_escape}
    -DscmCommentPrefix="${symbol_dollar}(cat "tmp/release_comments/${symbol_dollar}{RELEASE_VERSION}.txt")"'${symbol_dollar}{line.separator}${symbol_dollar}{line.separator}' ${symbol_escape}
    clean ${symbol_escape}
    release:prepare
```

References:

- [http://maven.apache.org/maven-release/maven-release-plugin/examples/prepare-release.html](http://maven.apache.org/maven-release/maven-release-plugin/examples/prepare-release.html)

${symbol_pound}${symbol_pound} `release:perform`

- checkout code with release tag given by prior release:prepare run into directory workingDirectory (default
  ${symbol_dollar}{project.build.directory}/checkout)
- within directory workingDirectory, execute Maven release goals (default deploy site-deploy)
    - compile classes
    - execute tests
    - build the jar file
    - create source jar file
    - create javadoc jar file
    - create checksums on artifacts
    - gpg sign artifacts
    - install the artifact in the local repository
    - deploy the artifacts to the libs-release repository of artifactory

```bash
mvn ${symbol_escape}
    release:perform

git pull
```

References:

[http://maven.apache.org/maven-release/maven-release-plugin/examples/perform-release.html](http://maven.apache.org/maven-release/maven-release-plugin/examples/perform-release.html)

${symbol_pound}${symbol_pound} `site`

- clean
- compile `src/main` classes
- compile `src/test` classes
- execute tests
- build the jar file
- create source jar file
- create javadoc jar file
- generate site javadoc documentation
- generate site cobertura coverage report

```bash
mvn ${symbol_escape}
    clean ${symbol_escape}
    site
```